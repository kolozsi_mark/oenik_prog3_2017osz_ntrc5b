﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Media;
using System.Windows.Media;

namespace oenik_prog3_2017osz_ntrc5b
{
    public class BackgroundValueConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int? ship = (int?)value;
            switch(ship)
            {
                case -1:return Brushes.LightBlue;
                case 0:return Brushes.Black;
                case 1:return Brushes.Green;
                case 2: return Brushes.Blue;
                case 3: return Brushes.Red;
                case 4:return Brushes.Orange;
                case null: return Brushes.LightBlue;
                default:return Brushes.LightBlue;
            }
        }   
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
