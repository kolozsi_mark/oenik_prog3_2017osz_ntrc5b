﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace oenik_prog3_2017osz_ntrc5b
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel VM;
        Player player1;
        Player player2;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult msr = MessageBox.Show("Biztos ki akarsz lépni?", "Kérdés", MessageBoxButton.YesNo);
            if (msr == MessageBoxResult.Yes)
            {
                
            }
            else
            {
                e.Cancel=true;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (tb_nev.Text != "")
            {
                bool? result;
                if (multi.IsChecked == true)
                {
                    MessageBox.Show(tb_nev.Text + ", te rakhatod le először a hajóid.");
                }
                player1 = new Player(tb_nev.Text);
                VM = new ViewModel(player1);
                VM.PlayerShipplace(player1,out result);
                if (multi.IsChecked == true && result==true)
                {
                    if (tb_nev2.Text != "")
                    {
                        MessageBox.Show(tb_nev2.Text + " következik.");
                        player2 = new Player(tb_nev2.Text);
                        VM = new ViewModel(player1, player2);
                        VM.PlayerShipplace(player2,out result);
                        if (result == true)
                        {
                            Game gm = new Game(player1, player2);
                            gm.ShowDialog();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Nem írtál be nevet!");
                    }
                }
                if(multi.IsChecked==false && result==true)
                {
                    Game gm = new Game(player1);
                    gm.ShowDialog();
                }
                
            }
            else
            {
                MessageBox.Show("Nem adtál meg nevet!");
            }
        }

        private void multi_Checked(object sender, RoutedEventArgs e)
        {
            lb_p2.Visibility = Visibility.Visible;
            tb_nev2.Visibility = Visibility.Visible;
            window.Height += 50;
        }

        private void single_Checked(object sender, RoutedEventArgs e)
        {
            lb_p2.Visibility = Visibility.Collapsed;
            tb_nev2.Visibility = Visibility.Collapsed;
            window.Height = 225.744;
        }
    }
}
