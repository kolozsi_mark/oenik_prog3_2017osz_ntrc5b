﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace oenik_prog3_2017osz_ntrc5b
{
    /// <summary>
    /// Interaction logic for ShipPlacer.xaml
    /// </summary>
    public partial class ShipPlacer : Window
    {
        int currentShip; //-1 none, 0 patrol boat, 1 submarine, 2 destroyer, 3 battleship, 4 aircraft carrier
        ViewModel VM;
        Player player;
        GameScreen gm;
        Ships Chosen;
        SolidColorBrush brush;
        int prevx = -1;
        int prevy = -1;
        bool boszlop = false;
        bool bsor = false;

        public void ChosenShip(Ships ship, int currentShip, int x, int y, ref int prevx, ref int prevy, SolidColorBrush br)
        {
            if (!ship.Finished())
            {
                if (player.ShipSet[x, y] == -1)
                {
                    if (ship.N >= 2)
                    {
                        bool kitesz = false;
                        while(!kitesz)
                        {
                            int oszlop = prevx - x; //egy oszlopban van e? 0-igen
                            int sor = prevy - y; //egy sorban van e? 0-igen
                            if (oszlop == 0 && boszlop == true)
                            {
                                foreach (var item in ship.PositionsY)
                                {
                                    if (item - y == 1)
                                    {
                                        player.ShipSet[x, y] = currentShip;
                                        ship.AddPosition(x, y);
                                        kitesz = true;
                                    }
                                    else if (item - y == -1)
                                    {
                                        player.ShipSet[x, y] = currentShip;
                                        ship.AddPosition(x, y);
                                        kitesz = true;
                                    }
                                }
                            }
                            if (sor == 0 && bsor == true)
                            {
                                foreach (var item in ship.PositionsX)
                                    {
                                        if (item - x == 1)
                                        {
                                            player.ShipSet[x, y] = currentShip;
                                            ship.AddPosition(x, y);
                                            kitesz = true;
                                        }
                                        else if (item - x == -1)
                                        {
                                            player.ShipSet[x, y] = currentShip;
                                            ship.AddPosition(x, y);
                                            kitesz = true;
                                        }
                                    }
                                }
                            else
                            {
                                kitesz = true;
                            }
                            }
                        }
                    if (ship.N == 1)
                    {
                        if (prevx == x && prevy == y - 1)
                        {
                            player.ShipSet[x, y] = currentShip;
                            ship.AddPosition(x, y);
                            boszlop = true;
                        }
                        else if (prevx == x && prevy == y + 1)
                        {
                            player.ShipSet[x, y] = currentShip;
                            ship.AddPosition(x, y);
                            boszlop = true;
                        }
                        else if (prevx == x - 1 && prevy == y)
                        {
                            player.ShipSet[x, y] = currentShip;
                            ship.AddPosition(x, y);
                            bsor = true;
                        }
                        else if (prevx == x + 1 && prevy == y)
                        {
                            player.ShipSet[x, y] = currentShip;
                            ship.AddPosition(x, y);
                            bsor = true;
                        }
                    }
                    if (ship.N == 0)
                    {
                        prevx = x;
                        prevy = y;
                        player.ShipSet[x, y] = currentShip;
                        ship.AddPosition(x, y);
                        switch (currentShip)
                        {
                            case 0: b_del_patrol.IsEnabled = true; break;
                            case 1: b_del_submarine.IsEnabled = true; break;
                            case 2: b_del_destroyer.IsEnabled = true; break;
                            case 3: b_del_battleship.IsEnabled = true; break;
                            case 4: b_del_aircraft.IsEnabled = true; break;
                            default:
                                break;
                        }
                    }
                }
                if (ship.Finished())
                {
                    switch (currentShip)
                    {
                        case 0: b_patrol.Content = "Done!"; b_del_patrol.IsEnabled = true; break;
                        case 1: b_submarine.Content = "Done!"; b_del_submarine.IsEnabled = true; break;
                        case 2: b_destroyer.Content = "Done!"; b_del_destroyer.IsEnabled = true; break;
                        case 3: b_battleship.Content = "Done!"; b_del_battleship.IsEnabled = true; break;
                        case 4: b_aircraft.Content = "Done!"; b_del_aircraft.IsEnabled = true;  break;
                        default:
                            break;
                    }
                    currentShip = -1;
                    ship = null;
                    if (player.patrol != null && player.submarine != null && player.destroyer != null && player.battleship != null && player.aircraft != null) if (player.patrol.Finished() && player.submarine.Finished() && player.destroyer.Finished() && player.battleship.Finished() && player.aircraft.Finished()) b_finish.IsEnabled = true;
                }
            }
        }


        public ShipPlacer(ViewModel VM,ref Player player)
        {
            this.VM = VM;
            InitializeComponent();
            currentShip = -1;
            gm = new GameScreen(player,true);
            canvas.Children.Add(gm);
            gm.MouseLeftButtonDown += Gm_MouseLeftButtonDown;
            this.player = player;
        }

        private void Gm_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (Chosen != null)
            {
                Point X = Mouse.GetPosition(canvas);
                int x = (int)X.X / 30;
                int y = (int)X.Y / 30;
                ChosenShip(Chosen, currentShip, x, y, ref prevx, ref prevy, brush);
                gm.InvalidateVisual();
            }
        }

        public bool finish_buttonclicked = false;
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (finish_buttonclicked == false)
            {
                MessageBoxResult msr = MessageBox.Show("Biztos ki akarsz lépni?", "Kérdés", MessageBoxButton.YesNo);
                if (msr == MessageBoxResult.Yes)
                {

                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        private void Buttonss(ref Ships ship,ref int currentship,ref SolidColorBrush brush, Button b_ship)
        {
            switch (currentship)
            {
                case 0: ship = new Ships(2); break;
                case 1: ship = new Ships(3); break;
                case 2: ship = new Ships(3); break;
                case 3: ship = new Ships(4); break;
                case 4: ship = new Ships(5); break;
            }
            Chosen = ship;
            switch(currentship)
            {
                case 0:brush= Brushes.Black;break;
                case 1:brush = Brushes.Green;break;
                case 2:brush = Brushes.Blue;break;
                case 3:brush = Brushes.Red;break;
                case 4:brush = Brushes.Orange;break;
            }
            b_ship.IsEnabled = false;
            boszlop = false;
            bsor = false;
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            currentShip = 0;
            Buttonss(ref player.patrol,ref currentShip,ref brush, b_patrol);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            currentShip = 1;
            Buttonss(ref player.submarine, ref currentShip, ref brush, b_submarine);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            currentShip = 2;
            Buttonss(ref player.destroyer, ref currentShip, ref brush, b_destroyer);
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            currentShip = 3;
            Buttonss(ref player.battleship, ref currentShip, ref brush, b_battleship);
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            currentShip = 4;
            Buttonss(ref player.aircraft, ref currentShip, ref brush, b_aircraft);
        }

        private void b_del_patrol_Click(object sender, RoutedEventArgs e)
        {
            b_patrol.Content = "Patrol";
            b_patrol.IsEnabled = true;
            b_finish.IsEnabled = false;
            b_del_patrol.IsEnabled = false;
            for (int i = 0; i < player.patrol.N; i++)
            {
                player.ShipSet[player.patrol.PositionsX[i], player.patrol.PositionsY[i]] = -1;
            }
            player.patrol = null;
            gm.InvalidateVisual();
            currentShip = -1;
        }

        private void b_del_submarine_Click(object sender, RoutedEventArgs e)
        {
            b_submarine.Content = "Submarine";
            b_submarine.IsEnabled = true;
            b_del_submarine.IsEnabled = false;
            b_finish.IsEnabled = false;
            for (int i = 0; i < player.submarine.N; i++)
            {
                player.ShipSet[player.submarine.PositionsX[i], player.submarine.PositionsY[i]] = -1;
            }
            player.submarine = null;
            gm.InvalidateVisual();
            currentShip = -1;

        }

        private void b_del_destroyer_Click(object sender, RoutedEventArgs e)
        {
            b_destroyer.Content = "Destroyer";
            b_destroyer.IsEnabled = true;
            b_del_destroyer.IsEnabled = false;
            for (int i = 0; i < player.destroyer.N; i++)
            {
                player.ShipSet[player.destroyer.PositionsX[i], player.destroyer.PositionsY[i]] = -1;
            }
            player.destroyer = null;
            b_finish.IsEnabled = false;
            gm.InvalidateVisual();
            currentShip = -1;
        }

        private void b_del_battleship_Click(object sender, RoutedEventArgs e)
        {
            b_battleship.Content = "Battleship";
            b_battleship.IsEnabled = true;
            b_del_battleship.IsEnabled = false;
            for (int i = 0; i < player.battleship.N; i++)
            {
                player.ShipSet[player.battleship.PositionsX[i], player.battleship.PositionsY[i]] = -1;
            }
            player.battleship = null;
            b_finish.IsEnabled = false;
            gm.InvalidateVisual();
            currentShip = -1;
        }

        private void b_del_aircraft_Click(object sender, RoutedEventArgs e)
        {
            b_aircraft.Content = "Aircraft";
            b_aircraft.IsEnabled = true;
            b_del_aircraft.IsEnabled = false;
            for (int i = 0; i < player.aircraft.N; i++)
            {
                player.ShipSet[player.aircraft.PositionsX[i], player.aircraft.PositionsY[i]] = -1;
            }
            player.aircraft = null;
            b_finish.IsEnabled = false;
            gm.InvalidateVisual();
            currentShip = -1;
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            finish_buttonclicked = true;
            MessageBoxResult mbr = MessageBox.Show("Biztos kész vagy?", "Kilépés", MessageBoxButton.YesNoCancel, MessageBoxImage.Information);
            if (mbr == MessageBoxResult.Yes)
            {
                DialogResult = true;
            }
            else if (mbr == MessageBoxResult.No)
            {
                DialogResult = false;
            }
            else
            {
                mbr = MessageBoxResult.Cancel;
            }
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            if (b_del_aircraft.IsEnabled == false && b_del_battleship.IsEnabled == false && b_del_destroyer.IsEnabled == false && b_del_patrol.IsEnabled == false && b_del_submarine.IsEnabled == false)
            {
                VM.VeletlenHajoGeneral(ref player);
                gm.InvalidateVisual();
                b_patrol.Content = "Done!";
                b_submarine.Content = "Done!";
                b_aircraft.Content = "Done!";
                b_battleship.Content = "Done!";
                b_destroyer.Content = "Done!";
                b_del_aircraft.IsEnabled = true;
                b_del_battleship.IsEnabled = true;
                b_del_destroyer.IsEnabled = true;
                b_del_patrol.IsEnabled = true;
                b_del_submarine.IsEnabled = true;
                b_patrol.IsEnabled = false;
                b_submarine.IsEnabled = false;
                b_destroyer.IsEnabled = false;
                b_aircraft.IsEnabled = false;
                b_battleship.IsEnabled = false;
                b_finish.IsEnabled = true;
            }
            else
            {
                MessageBox.Show("Előbb minden hajót törölj le!");
            }
        }
    }
}

