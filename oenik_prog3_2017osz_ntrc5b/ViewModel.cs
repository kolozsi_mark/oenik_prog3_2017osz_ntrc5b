﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace oenik_prog3_2017osz_ntrc5b
{
    public enum JatekVegeAllapot
    {
        jatekos1nyert, jatekos2nyert
    }
    public class JatekVegeEventArgs : EventArgs
    {
        JatekVegeAllapot jatekvege;

        public JatekVegeAllapot Jatekvege
        {
            get
            {
                return jatekvege;
            }

            set
            {
                jatekvege = value;
            }
        }
        public JatekVegeEventArgs(JatekVegeAllapot allapot)
        {
            this.jatekvege = allapot;
        }
    }
    public class ViewModel:Bindable
    {
        public JatekVegeEventArgs vege;
        public Player player1;
        public Player player2;

        public ViewModel(Player player1)
        {
            this.player1 = player1;
            this.player2 = new Player("AI");
            this.VeletlenHajoGeneral(ref player2);
        }

        public ViewModel(Player player1, Player player2)
        {
            this.player1 = player1;
            this.player2 = player2;
        }

        public void PlayerShipplace(Player player,out bool? x)
        {
            ShipPlacer p = new ShipPlacer(this,ref player);
            p.ShowDialog();
            x= p.DialogResult;
        }
        public void VeletlenHajoGeneral(ref Player player)
        {
            AIShips(player);
            AIShips2(player);
        }

        public void AIShips(Player p2)
        {
            Random r = new Random();
            int[] j = new int[5];
            j[0] = 2;
            j[1] = j[2] = 3;
            j[3] = 4;
            j[4] = 5;
            p2.patrol = new Ships(2);
            p2.submarine = new Ships(3);
            p2.destroyer = new Ships(3);
            p2.battleship = new Ships(4);
            p2.aircraft = new Ships(5);
            for (int i = 0; i < 5; i++)
            {
                int rotate = r.Next(0, 2);
                if (rotate == 0) //oszlop
                {
                    AIShips1(p2,i, j[i], false);
                }
                else //sor
                {
                    AIShips1(p2,i, j[i], true);
                }
            }
        }
        public void AIShips2(Player p2)
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    switch (p2.ShipSet[i, j])
                    {
                        case 0: p2.patrol.AddPosition(i, j); break;
                        case 1: p2.submarine.AddPosition(i, j); break;
                        case 2: p2.destroyer.AddPosition(i, j); break;
                        case 3: p2.battleship.AddPosition(i, j); break;
                        case 4: p2.aircraft.AddPosition(i, j); break;
                    }
                }
            }
        }
        public void AIShips1(Player p2,int currentship, int currentlength, bool rotate)
        {
            Random r1 = new Random();
            int x, y;
            bool lerakva = false;
            if (rotate == false)
            {
                while (!lerakva)
                {
                    int z = 0;
                    x = r1.Next(0, 10 - currentlength);
                    y = r1.Next(0, 10);
                    bool van = false;
                    while (z < currentlength && !van)
                    {
                        if (p2.ShipSet[x + z, y] == -1)
                        {
                            z++;
                        }
                        else
                        {
                            van = true;
                        }
                    }
                    if (z == currentlength)
                    {
                        for (int t = 0; t < currentlength; t++)
                        {
                            p2.ShipSet[x + t, y] = currentship;
                        }
                        lerakva = true;
                    }
                    else
                    {
                        RandomGeneral(ref r1);
                    }
                }
            }
            else
            {
                while (!lerakva)
                {
                    int z = 0;
                    x = r1.Next(0, 10);
                    y = r1.Next(0, 10 - currentlength);
                    bool van = false;
                    while (z < currentlength && !van)
                    {
                        if (p2.ShipSet[x, y + z] == -1)
                        {
                            z++;
                        }
                        else
                        {
                            van = true;
                        }
                    }
                    if (z == currentlength)
                    {
                        for (int t = 0; t < currentlength; t++)
                        {
                            p2.ShipSet[x, y + t] = currentship;
                        }
                        lerakva = true;
                    }
                    else
                    {
                        RandomGeneral(ref r1);
                    }
                }
            }

        }
        public void RandomGeneral(ref Random r1)
        {
            r1 = new Random();
        }
    }
}
