﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oenik_prog3_2017osz_ntrc5b
{
    public class Ships:Bindable
    {
        int length;
        int[] positionsX;
        int[] positionsY;
        int n;
        bool sink;
        public Ships(int length)
        {
            this.Length = length;
            this.PositionsX = new int[length];
            this.PositionsY = new int[length];
            Pos(PositionsX);
            Pos(PositionsY);
            this.sink = false;
            N = 0;
        }
        public void Pos(int[] pos)
        {
            for (int i = 0; i < length; i++)
            {
                pos[i] = -1;
            }
        }
        public int Length
        {
            get
            {
                return length;
            }

            set
            {
                length = value;
            }
        }
        public int[] PositionsX
        {
            get
            {
                return positionsX;
            }

            set
            {
                
                positionsX  = value;
            }
        }

        public int[] PositionsY
        {
            get
            {
                return positionsY;
            }

            set
            {
                positionsY = value;
            }
        }

        public int N
        {
            get
            {
                return n;
            }

            set
            {
                n = value;
            }
        }

        public bool Sink
        {
            get
            {
                if(length==0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            set
            {
                sink = value;
            }
        }

        public void AddPosition(int x,int y)
        {
            PositionsX[N] = x;
            PositionsY[N] = y;
            N++;
        }
        public bool Finished()
        {
            if(n==length)
            {
                return true;
            }
            return false;
        }
    }
}
