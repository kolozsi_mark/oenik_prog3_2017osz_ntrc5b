﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace oenik_prog3_2017osz_ntrc5b
{
    /// <summary>
    /// Interaction logic for Game.xaml
    /// </summary>
    public partial class Game : Window
    {
        ViewModel VM;
        Player p1;
        Player p2;

        Random r1 = new Random();
        Random r2 = new Random();
        ObservableCollection<string> p1_Hits = new ObservableCollection<string>();
        ObservableCollection<string> p2_Hits = new ObservableCollection<string>();
        bool ok = true;
        GameScreen play1;
        GameScreen play2;
        MessageBoxResult mbr_p1 = MessageBoxResult.Yes;
        MessageBoxResult mbr_p2 = MessageBoxResult.Yes;
        char[] abc = { '0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' };

        public event EventHandler<JatekVegeEventArgs> vege;

        public Game(Player p1)
        {
            InitializeComponent();
            this.p1 = p1;
            VM = new ViewModel(p1);
            this.p2 = VM.player2;
            play1 = new GameScreen(p1, false);
            play2 = new GameScreen(p2, false);
            play2.MouseLeftButtonDown += Play1_MouseLeftButtonDown;
            c_p1.Children.Add(play1);
            c_p2.Children.Add(play2);
            lb_nev1.Content = p1.Name + " táblája";
            lb_nev2.Content = p2.Name + " táblája";
            label_p1name.Content = p2.Name + " lövései";
            label_p2name.Content = p1.Name + " lövései";


        }

        private void Play1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (pb_1.Value < 5 && pb_2.Value < 5)
            {
                if (p1.Try == p2.Try)
                {
                    Turn1(ref mbr_p1);
                    if (mbr_p1 == MessageBoxResult.Yes)
                    {
                        play2.InvalidateVisual();
                        if (p2.Name == "AI")
                        {
                            Turn2AI();
                            play1.InvalidateVisual();
                        }
                        else
                        {
                            MessageBox.Show(p2.Name + " következik.");
                        }
                    }
                    mbr_p1 = MessageBoxResult.Yes;
                }
                else
                {
                    Turn2(ref mbr_p2);
                    play1.InvalidateVisual();
                    if (mbr_p2 == MessageBoxResult.Yes)
                    {
                        MessageBox.Show(p1.Name + " következik.");
                    }
                }
            }
        }

        public Game(Player p1, Player p2)
        {
            InitializeComponent();

            this.p1 = p1;
            this.p2 = p2;
            VM = new ViewModel(p1, p2);
            play1 = new GameScreen(p1, false);
            play1.MouseLeftButtonDown += Play1_MouseLeftButtonDown;
            play2 = new GameScreen(p2, false);
            play2.MouseLeftButtonDown += Play1_MouseLeftButtonDown;
            c_p1.Children.Add(play1);
            c_p2.Children.Add(play2);
            lb_nev1.Content = p1.Name + " táblája";
            label_p1name.Content = p1.Name + " lövései";
            lb_nev2.Content = p2.Name + " táblája";
            label_p2name.Content = p2.Name + " lövései";
        }

        public void RandomGeneral(ref Random r1, ref Random r2)
        {
            r1 = new Random();
            r2 = new Random();
        }
        public void Turn(Canvas c_p, Canvas c_p2, Player p_me, Player p_enemy, ListBox lb, ProgressBar pb, ref MessageBoxResult mbr, Label trys, Label hits, Label ratio)
        {
            mbr = MessageBoxResult.Yes;
            Point X = Mouse.GetPosition(c_p);
            int x = (int)X.X / 30 - 1;
            int y = (int)X.Y / 30 - 1;
            Point Y = Mouse.GetPosition(c_p2);
            int x2 = (int)Y.X / 30 - 1;
            int y2 = (int)Y.Y / 30 - 1;
            if (!(x2 > -1 && x2 < 10 && y2 > -1 && y2 < 10))
            {
                while (p_enemy.RevealedCells[x, y] != true)
                {
                    if (p_enemy.ShipSet[x, y] != -1)
                    {
                        p_me.Hits++;
                        int z = p_enemy.ShipSet[x, y];
                        switch (z) //0 -> -2 patrol, 1 -> -3 submarine, 2 -> -4 destroyer, 3 -> -5 battleship, 4 -> -6 aircraft
                        {
                            case 0:
                                p_enemy.patrol.Length--;
                                p_enemy.ShipSet[x, y] = -2;
                                if (p_enemy.patrol.Sink == true)
                                {
                                    pb.Value++;
                                }
                                break;
                            case 1:
                                p_enemy.submarine.Length--;
                                p_enemy.ShipSet[x, y] = -3;
                                if (p_enemy.submarine.Sink == true)
                                {
                                    pb.Value++;
                                }
                                break;
                            case 2:
                                p_enemy.destroyer.Length--;
                                p_enemy.ShipSet[x, y] = -4;
                                if (p_enemy.destroyer.Sink == true)
                                {
                                    pb.Value++;
                                }
                                break;
                            case 3:
                                p_enemy.battleship.Length--;
                                p_enemy.ShipSet[x, y] = -5;
                                if (p_enemy.battleship.Sink == true)
                                {
                                    pb.Value++;
                                }
                                break;
                            case 4:
                                p_enemy.aircraft.Length--;
                                p_enemy.ShipSet[x, y] = -6;
                                if (p_enemy.aircraft.Sink == true)
                                {
                                    pb.Value++;
                                }
                                break;
                            default: break;
                        }
                        p_enemy.RevealedCells[x, y] = true;
                        p_me.Try++;
                        Label item = new Label();
                        item.Width = lb.Width;
                        item.Content = (x + 1) + " " + abc[y + 1];
                        item.Background = Brushes.LightPink;
                        lb.Items.Add(item);
                        trys.Content = "Lövések: " + p_me.Try;
                        hits.Content = "Találat: " + p_me.Hits;
                        p_me.HitRatio = (double)p_me.Hits / p_me.Try;
                        ratio.Content = "Ratio: " + Math.Round(p_me.HitRatio, 2) * 100 + "%";
                    }
                    else
                    {
                        p_me.Misses++;
                        p_enemy.RevealedCells[x, y] = true;
                        p_me.Try++;
                        lb.Items.Add((x + 1) + " " + abc[y + 1]);
                        trys.Content = "Lövések: " + p_me.Try;
                        hits.Content = "Találat: " + p_me.Hits;
                        p_me.HitRatio = (double)p_me.Hits / p_me.Try;
                        ratio.Content = "Ratio: " + Math.Round(p_me.HitRatio, 2) * 100 + "%";
                    }
                }
                if (p_enemy.RevealedCells[x, y] == true && p_enemy.Try == p_me.Try)
                {
                    mbr = MessageBoxResult.No;
                }
            }
            else
            {
                MessageBox.Show("Rossz helyre nyomtál!");
                mbr = MessageBoxResult.No;
            }
        }
        public void Turn1(ref MessageBoxResult mbr_p1)
        {
            Turn(c_p2, c_p1, p1, p2, lb_2, pb_1, ref mbr_p1, label_p1try, label_p1hits, label_p1ratio);
            if (pb_1.Value == 5)
            {
                vege?.Invoke(this, new JatekVegeEventArgs(JatekVegeAllapot.jatekos1nyert));
                MessageBox.Show(p1.Name + " nyertél!!!");
                MessageBoxResult mbr = MessageBox.Show("Szeretnél újra játszani?", "Info", MessageBoxButton.YesNo);
                if (mbr == MessageBoxResult.Yes)
                {
                    MainWindow xy = new MainWindow();
                    xy.ShowDialog();
                }
                else
                {
                    this.Close();
                }
            }
        }
        public void Turn2(ref MessageBoxResult mbr_p2)
        {
            Turn(c_p1, c_p2, p2, p1, lb_1, pb_2, ref mbr_p2, label_p2try, label_p2hits, label_p2ratio);
            if (pb_2.Value == 5)
            {
                vege?.Invoke(this, new JatekVegeEventArgs(JatekVegeAllapot.jatekos1nyert));
                MessageBox.Show(p2.Name + " nyertél!!!");
                MessageBoxResult mbr = MessageBox.Show("Szeretnél újra játszani?", "Info", MessageBoxButton.YesNo);
                if (mbr == MessageBoxResult.Yes)
                {
                    System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                    Application.Current.Shutdown();
                }
                else
                {
                    this.Close();
                }
            }
        }
        public void Turn2AI()
        {
            bool rakott = false;
            while (!rakott)
            {
                RandomGeneral(ref r1, ref r2);
                int x = r1.Next(0, 10);
                int y = r1.Next(0, 10);
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        if (p1.ShipSet[i, j] < -1)
                        {
                            switch (p1.ShipSet[i, j])
                            {
                                case -2:
                                    if (p1.patrol.Sink == true) { ok = true; }
                                    else
                                    {
                                        ok = false;
                                    }
                                    break;
                                case -3:
                                    if (p1.submarine.Sink == true) { ok = true; }
                                    else
                                    {
                                        ok = false;
                                    }
                                    break;
                                case -4:
                                    if (p1.destroyer.Sink == true) { ok = true; }
                                    else { ok = false; }
                                    break;
                                case -5:
                                    if (p1.battleship.Sink == true) { ok = true; }
                                    else { ok = false; }
                                    break;
                                case -6:
                                    if (p1.aircraft.Sink == true) { ok = true; }
                                    else
                                    {
                                        ok = false;
                                    }
                                    break;
                            }

                        }
                    }
                }
                //0-> -2 patrol 1 -> -3 submarine, 2 -> -4 destroyer, 3 -> -5 battleship, 4 -> -6 aircraft
                while (ok != true) //van e meglőtt hajó true - nincs, false - van
                {
                    int i = 0;
                    int j = 0;
                    while (i < 10 && ok != true)
                    {
                        j = 0;
                        while (j <= 9 && ok != true)
                        {
                            if (p1.ShipSet[i, j] < -1)
                            {
                                bool skip = false;
                                if (i == 0 && j == 0)
                                {
                                    if (p1.RevealedCells[i + 1, j] == true && p1.RevealedCells[i, j + 1] == true)
                                    {
                                        skip = true;
                                    }
                                }
                                else if (i == 9 && j == 0)
                                {
                                    if (p1.RevealedCells[i - 1, j] == true && p1.RevealedCells[i, j + 1] == true)
                                    {
                                        skip = true;
                                    }
                                }
                                else if (i == 0 && j == 9)
                                {
                                    if (p1.RevealedCells[i + 1, j] == true && p1.RevealedCells[i, j - 1] == true)
                                    {
                                        skip = true;
                                    }
                                }
                                else if (i == 9 && j == 9)
                                {
                                    if (p1.RevealedCells[i - 1, j] == true && p1.RevealedCells[i, j - 1] == true)
                                    {
                                        skip = true;
                                    }
                                }
                                else if (i == 0)
                                {
                                    if (p1.RevealedCells[i, j + 1] == true && p1.RevealedCells[i, j - 1] == true && p1.RevealedCells[i + 1, j] == true)
                                    {
                                        skip = true;
                                    }
                                }
                                else if (i == 9)
                                {
                                    if (p1.RevealedCells[i, j + 1] == true && p1.RevealedCells[i, j - 1] == true && p1.RevealedCells[i - 1, j] == true)
                                    {
                                        skip = true;
                                    }
                                }
                                else if (j == 0)
                                {
                                    if (p1.RevealedCells[i + 1, j] == true && p1.RevealedCells[i - 1, j] == true && p1.RevealedCells[i, j + 1] == true)
                                    {
                                        skip = true;
                                    }
                                }
                                else if (j == 9)
                                {
                                    if (p1.RevealedCells[i + 1, j] == true && p1.RevealedCells[i - 1, j] == true && p1.RevealedCells[i, j - 1] == true)
                                    {
                                        skip = true;
                                    }
                                }
                                else if (i - 1 >= 0 && p1.RevealedCells[i - 1, j] == true &&
                                        i + 1 <= 9 && p1.RevealedCells[i + 1, j] == true &&
                                        j - 1 >= 0 && p1.RevealedCells[i, j - 1] == true &&
                                        j + 1 <= 9 && p1.RevealedCells[i, j + 1] == true)
                                {
                                    skip = true;
                                }
                                if (p1.RevealedCells[i, j] == true && !skip)
                                {
                                    bool jorandom = false;
                                    while (!jorandom && ok != true)
                                    {
                                        int rand = r1.Next(1, 5);
                                        if (rand == 1) //bal
                                        {
                                            if (i - 1 >= 0 && p1.RevealedCells[i - 1, j] != true)
                                            {
                                                x = i - 1;
                                                y = j;
                                                ok = true;
                                                jorandom = true;
                                            }
                                        }
                                        if (rand == 2) //jobb
                                        {
                                            if (i + 1 <= 9 && p1.RevealedCells[i + 1, j] != true)
                                            {
                                                x = i + 1;
                                                y = j;
                                                ok = true;
                                                jorandom = true;
                                            }
                                        }
                                        if (rand == 3) //lent
                                        {
                                            if (j - 1 >= 0 && p1.RevealedCells[i, j - 1] != true)
                                            {
                                                y = j - 1;
                                                x = i;
                                                ok = true;
                                                jorandom = true;
                                            }
                                        }
                                        if (rand == 4) //fent
                                        {
                                            if (j + 1 <= 9 && p1.RevealedCells[i, j + 1] != true)
                                            {
                                                y = j + 1;
                                                x = i;
                                                ok = true;
                                                jorandom = true;
                                            }
                                        }
                                    }
                                }
                            }
                            j++;
                        }
                        i++;
                    }
                }
                while (p1.RevealedCells[x, y] != true)
                {
                    if (p1.ShipSet[x, y] != -1)
                    {
                        p2.Hits++;
                        int z = p1.ShipSet[x, y];
                        switch (z) //0 -> -2 patrol, 1 -> -3 submarine, 2 -> -4 destroyer, 3 -> -5 battleship, 4 -> -6 aircraft
                        {
                            case 0:
                                p1.patrol.Length--;
                                p1.ShipSet[x, y] = -2;
                                if (p1.patrol.Sink == true)
                                {
                                    pb_2.Value++;
                                    rakott = true;
                                }
                                else
                                {
                                    ok = false;
                                    rakott = true;
                                }
                                break;
                            case 1:
                                p1.submarine.Length--;
                                p1.ShipSet[x, y] = -3;
                                if (p1.submarine.Sink == true)
                                {
                                    pb_2.Value++;
                                    rakott = true;
                                }
                                else
                                {
                                    ok = false;
                                    rakott = true;
                                }
                                break;
                            case 2:
                                p1.destroyer.Length--;
                                p1.ShipSet[x, y] = -4;
                                if (p1.destroyer.Sink == true)
                                {
                                    pb_2.Value++;
                                    rakott = true;
                                }
                                else
                                {
                                    ok = false;
                                    rakott = true;
                                }
                                break;
                            case 3:
                                p1.battleship.Length--;
                                p1.ShipSet[x, y] = -5;
                                if (p1.battleship.Sink == true)
                                {
                                    pb_2.Value++;
                                    rakott = true;
                                }
                                else
                                {
                                    ok = false;
                                    rakott = true;
                                }
                                break;
                            case 4:
                                p1.aircraft.Length--;
                                p1.ShipSet[x, y] = -6;
                                if (p1.aircraft.Sink == true)
                                {
                                    pb_2.Value++;
                                    rakott = true;
                                }
                                else
                                {
                                    ok = false;
                                    rakott = true;
                                }
                                break;
                            default: break;
                        }
                        p1.RevealedCells[x, y] = true;
                        p2.Try++;

                        Label item = new Label();
                        item.Width = lb_1.Width;
                        item.Content = (x + 1) + " " + abc[y + 1];
                        item.Background = Brushes.LightPink;
                        lb_1.Items.Add(item);
                        lb_1.SelectedIndex = lb_1.Items.Count - 1;
                        label_p2try.Content = "Lövések: " + p2.Try;
                        label_p2hits.Content = "Találat: " + p2.Hits;
                        p2.HitRatio = (double)p2.Hits / p2.Try;
                        label_p2ratio.Content = "Ratio: " + Math.Round(p2.HitRatio, 2) * 100 + "%";
                    }
                    else
                    {
                        p2.Misses++;
                        p1.RevealedCells[x, y] = true;
                        p2.Try++;
                        lb_1.Items.Add((x + 1) + " " + abc[y + 1]);
                        lb_1.SelectedIndex = lb_1.Items.Count - 1;
                        label_p2try.Content = "Lövések: " + p2.Try;
                        label_p2hits.Content = "Találat: " + p2.Hits;
                        p2.HitRatio = (double)p2.Hits / p2.Try;
                        label_p2ratio.Content = "Ratio: " + Math.Round(p2.HitRatio, 2) * 100 + "%";
                        rakott = true;
                    }
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (p2.Name == "AI")
            {
                MessageBox.Show("Kezdhetsz.");
            }
            else
            {
                MessageBox.Show(p1.Name + " kezd.");
            }
        }
    }
}
