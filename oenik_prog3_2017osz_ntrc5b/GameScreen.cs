﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace oenik_prog3_2017osz_ntrc5b
{
    class GameScreen : FrameworkElement
    {
        Player p;
        bool form;

        public GameScreen(Player p, bool form)
        {
            this.p = p;
            this.form = form;
        }

        protected override void OnRender(DrawingContext drawingcontext)
        {
            char[] abc = { '0','A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I' ,'J'};
            for (int i = 0; i < 11; i++)
            {
                for (int j = 0; j < 11; j++)
                {
                    if (form == true)
                    {
                        if(j==10 || i==10)
                        {
                            break;
                        }
                        switch (p.ShipSet[i, j])
                        {
                            case -1: drawingcontext.DrawRectangle(Brushes.LightBlue, new Pen(Brushes.Black,1), new Rect((i) * 30, (j) * 30, 30, 30)); break;
                            case 0: drawingcontext.DrawRectangle(Brushes.Black, new Pen(Brushes.White, 1), new Rect((i) * 30, (j) * 30, 30, 30)); break;
                            case 1: drawingcontext.DrawRectangle(Brushes.Green, new Pen(Brushes.Black, 1), new Rect((i) * 30, (j ) * 30, 30, 30)); break;
                            case 2: drawingcontext.DrawRectangle(Brushes.Blue, new Pen(Brushes.Black, 1), new Rect((i) * 30, (j) * 30, 30, 30)); break;
                            case 3: drawingcontext.DrawRectangle(Brushes.Red, new Pen(Brushes.Black, 1), new Rect((i) * 30, (j ) * 30, 30, 30)); break;
                            case 4: drawingcontext.DrawRectangle(Brushes.Orange, new Pen(Brushes.Black, 1), new Rect((i ) * 30, (j) * 30, 30, 30)); break;
                        }
                    }
                    else
                    {
                        if (i == 0 && j!=0)
                        {
                            drawingcontext.DrawRectangle(Brushes.DarkGray, new Pen(Brushes.Black, 1), new Rect(i * 30, j * 30, 30, 30));
                            drawingcontext.DrawText(new FormattedText(abc[j].ToString(), CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight,new Typeface("Verdena"),12,Brushes.Black), new Point(15,(j*30)+15));
                        }
                        else if(j==0 && i!=0)
                        {
                            drawingcontext.DrawRectangle(Brushes.DarkGray, new Pen(Brushes.Black, 1), new Rect(i * 30, j * 30, 30, 30));
                            drawingcontext.DrawText(new FormattedText(i.ToString(), CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Verdena"), 12, Brushes.Black), new Point((i*30)+15,  15));
                        }
                        else if (i != 0 && j!=0)
                        {
                            if (p.RevealedCells[i - 1, j - 1] == false)
                            {
                                drawingcontext.DrawRectangle(Brushes.LightGray, new Pen(Brushes.Black, 1), new Rect((i) * 30, (j) * 30, 30, 30));
                            }
                            else if (p.RevealedCells[i - 1, j - 1] == true)
                            {
                                switch (p.ShipSet[i - 1, j - 1])
                                {
                                    case -1: drawingcontext.DrawRectangle(Brushes.LightBlue, new Pen(Brushes.Black, 1), new Rect((i) * 30, (j) * 30, 30, 30)); break;
                                    case -2: SinkedShip(p.patrol, drawingcontext, (i ), (j )); break;
                                    case -3: SinkedShip(p.submarine, drawingcontext, (i), (j )); break;
                                    case -4: SinkedShip(p.destroyer, drawingcontext, (i ), (j )); break;
                                    case -5: SinkedShip(p.battleship, drawingcontext, (i ), (j )); break;
                                    case -6: SinkedShip(p.aircraft, drawingcontext, (i ), (j )); break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void SinkedShip(Ships ship,DrawingContext drawingcontext,int i,int j)
        {
            if(ship.Sink)
            {
                drawingcontext.DrawRectangle(Brushes.Pink, new Pen(Brushes.Black, 1), new Rect(i * 30, j * 30, 30, 30));
                drawingcontext.DrawLine(new Pen(Brushes.Black, 2), new Point(i * 30 + 5, j * 30 + 5), new Point(i * 30 + 25, j * 30 + 25));
                drawingcontext.DrawLine(new Pen(Brushes.Black, 2), new Point(i * 30 + 5, j * 30 + 25), new Point(i * 30 + 25, j * 30 + 5));
            }
            else
            {
                drawingcontext.DrawRectangle(Brushes.Purple, new Pen(Brushes.Black, 1), new Rect(i * 30, j * 30, 30, 30));
                drawingcontext.DrawEllipse(Brushes.White, new Pen(Brushes.Black, 1), new Point(i * 30 + 15, j * 30 + 15), 7.5, 7.5);
            }
        }
    }
}
