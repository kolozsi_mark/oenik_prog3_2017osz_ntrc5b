﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace oenik_prog3_2017osz_ntrc5b
{
    public class Player : Bindable
    {
        int hits;
        public string Name { get; set; }
        public int[,] ShipSet { get; set; }
        public bool[,] RevealedCells { get; set; }
        public int Hits
        {
            get
            {
                return hits;
            }
            set
            {
                hits = value;
                OPC();
            }
        }
        public int Misses { get; set; }
        public double HitRatio { get; set; }
        public int ShipLeft { get; set; }
        public int Try { get; set; }
        public Ships patrol;
        public Ships submarine;
        public Ships destroyer;
        public Ships battleship;
        public Ships aircraft;

        public Player(string name)
        {
            this.Name = name;
            hits = 0;
            Misses = 0;
            HitRatio = 0;
            Try = 0;
            ShipLeft = 0;
            ShipSet = new int[10, 10];
            RevealedCells = new bool[10, 10];
            for(int i=0;i<10;i++)
            {
                for(int j=0;j<10;j++)
                {
                    ShipSet[i, j] = -1;
                    RevealedCells[i,j] = false;
                }
            }
            patrol = new Ships(2);
            submarine = new Ships(3);
            destroyer = new Ships(3);
            battleship = new Ships(4);
            aircraft = new Ships(5);
            OPC();
        }
    }
}
